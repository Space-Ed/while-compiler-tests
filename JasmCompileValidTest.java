package tests;

import org.junit.Assert;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.internal.runners.statements.Fail;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;
import org.junit.runners.MethodSorters;
import whilelang.ast.WhileFile;
import whilelang.compiler.ClassFileWriter;
import whilelang.compiler.WhileCompiler;

import java.io.*;
import java.util.*;

import static org.junit.Assert.fail;

@RunWith(Parameterized.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class JasmCompileValidTest {

	static ArrayList<String> getTestDirectories(){
		//get all the directories for the

		String testDir = System.getenv("TEST_DIR");
		String testSet = System.getenv("TEST_SET");
		String[] tests = testSet.split(",");
		ArrayList<String> result = new ArrayList<String>();


		for (int i = 0; i < tests.length; i++) {
			result.add((testDir+"/"+ tests[i] + "/").replace('/', File.separatorChar));
		}

		return result;
	}


	// Here we enumerate all available test cases.
	@Parameters(name = "{0}")
	public static Collection<Object[]> data() {


		Collection<Object[]> testcases = new ArrayList<Object[]>();

		for (String dir : getTestDirectories()) {

			for (File f : new File(dir).listFiles()) {
				if (f.isFile()) {
					String name = f.getName();
					if (name.endsWith(".while")) {
						// Get rid of ".while" extension
						String testName = name.substring(0, name.length() - 6);
						testcases.add(new Object[] { testName, dir });
					}
				}
			}
		}
		return testcases;

	}

	private final String testName;
	private final String testDir;
	private final String stdoutfile;
	private final String stderrfile;
	private final String binaryFile;

	private WhileFile parsedAST;

	public JasmCompileValidTest(String testName, String testDir) {

		this.testName = testName;
		this.testDir = testDir;

		stdoutfile = testDir+"/Out/"+testName+".stdout";
		stderrfile = testDir+"/Out/"+testName+".stderr";
		binaryFile = testDir+"bin/"+testName+".class";

	}

	@Test
	public void runTest() throws IOException {
		parseWhileFile();
		compileWhileToJavaBytecode();
		exectuteCompiledClass();
	}

	private void parseWhileFile() throws IOException {

		//Compile
		String testFile = testDir + testName + ".while";
		WhileCompiler compiler = new WhileCompiler(testFile);

		//Will fail on syntax / static analysis
		parsedAST = compiler.compile();

	}

	private void compileWhileToJavaBytecode(){

		//Run bytecode generation
		try {
			ClassFileWriter jasmGenerator = new ClassFileWriter(binaryFile);
			jasmGenerator.write(parsedAST);

			System.out.println("[JasmCompileValidTest] generatedClassFile(Disassembled): \n" + disassembleBytecode());

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private String disassembleBytecode() throws IOException{

		// prepare execution command
		List<String> command = new ArrayList<String>();
		command.add("javap");
		command.add("-v");
		command.add(binaryFile);

		ProcessBuilder builder = new ProcessBuilder(command);
		//execute compiled while program
		final Process process = builder.start();

		List<String> stdout = readLines(process.getInputStream());
		return String.join("\n", stdout);
	}

	private void exectuteCompiledClass() throws IOException{

		// prepare execution command
		List<String> command = new ArrayList<String>();
		command.add("java");
		command.add("-cp");
		command.add(testDir+"/bin");
		command.add(testName);

		ProcessBuilder builder = new ProcessBuilder(command);
		//execute compiled while program
		final Process process = builder.start();

		try {
			final int i = process.waitFor();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		int exitStatus = process.exitValue();
		System.out.println("[JasmCompileValidTest] Process exit with status: "+ exitStatus);

		//extract command output/error
		List<String> stdout = readLines(process.getInputStream());
		List<String> stderr = readLines(process.getErrorStream());

		List<String> stdoutexpected = readLinesFromFile(stdoutfile);
		List<String> stderrexpected = readLinesFromFile(stderrfile);

		//if we expect output expect no error and the correct output
		if(stdoutexpected !=null){
			if(stderr.size() > 0){
				fail("[JasmCompileValidTest] byetecode execution ERROR: \nexpected no std err but found:\n    "+ String.join("\n    ", stderr));
			}
			assertStringSequencesEqual(stdoutexpected, stdout);
		}

		//if we expect error expect the correct error
		if(stderrexpected != null){
			assertStringSequencesEqual(stderrexpected, stderr);
		}

	}

	private List<String> readLinesFromFile(String file) throws IOException{
		try {
			return readLines(new FileInputStream(file));
		} catch (FileNotFoundException e) {
			return null;
		}
	}

	private List<String> readLines(InputStream stream) throws IOException{
		InputStreamReader isr = new InputStreamReader(stream);
		BufferedReader br = new BufferedReader(isr);
		ArrayList<String> lines = new ArrayList<String>();
		String line;
		while ((line = br.readLine()) != null) {
			lines.add(line);
		}

		return lines;
	}

	private void assertStringSequencesEqual(List<String> expectedLines, List<String> actualLines){
		//walk through side by side asserting equality
		int i = 0;
		while (i < Math.max(expectedLines.size(), actualLines.size())) {
			if (actualLines.size() <= i) fail("[JasmCompileValidTest] lines expected but not printed:\n---| "+String.join("\n---| ", expectedLines.subList(i, expectedLines.size())));
			if (expectedLines.size() <= i) fail("[JasmCompileValidTest] lines printed but not expected:\n---| "+String.join("\n---| ", actualLines.subList(i, actualLines.size())));

			Assert.assertEquals(expectedLines.get(i), actualLines.get(i));
			i = i+1;
		}
	}
}
